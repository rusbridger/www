---
title: "Robert Wu (吳才銓)"
email: "rupert[at]{cs[dot]toronto[dot]edu, together[dot]ai}"
pages: "[[about]](../index.html) [projects] [[cv]](../cv.pdf)"
links: "{ \
  [github](https://github.com/rhubarbwu), \
  [gitlab](https://gitlab.com/rhubarbwu), \
  [linkedin](https://www.linkedin.com/in/wu-robert), \
  [scholar](https://scholar.google.com/citations?user=X0M3q_sAAAAJ&hl=en), \
  }"
---

## Publications \& Pre-Prints

1. **Linguistic Collapse: Neural Collapse in (Large) Language Models**<br/>
   <u>Robert Wu</u>, [Vardan Papyan](https://sites.google.com/view/vardan-papyan)<br/>
   _NeurIPS 2024_ <br/>
   [**\[proceedings\]**](https://papers.nips.cc/paper_files/paper/2024/hash/f88cc8930b47a45ec4733123bf3039b9-Abstract-Conference.html)
   [**\[arxiv\]**](https://arxiv.org/abs/2405.17767)
   [**\[code\]**](https://github.com/rhubarbwu/linguistic-collapse)
2. **SEE-2-SOUND: Zero-Shot Spatial Environment-to-Spatial Sound**<br/>
   [Rishit Dagli](https://rishitdagli.com), [Shivesh Prakash](https://shivesh777.github.io/), <u>Robert Wu</u>, [Houman Khosravani](https://www.ncrit.org)<br/>
   [**\[arxiv\]**](https://arxiv.org/abs/2406.06612)
   [**\[code\]**](https://github.com/see2sound/see2sound)
3. **Towards One Shot Search Space Poisoning in Neural Architecture Search**<br/>
   [Nayan Saxena](https://uoft.me/nayan), <u>Robert Wu</u>, Rohan Jain<br/>
   _AAAI 2022 (Student Abstract+Poster)_<br/>
   [**\[proceedings\]**](https://ojs.aaai.org/index.php/AAAI/article/view/21658)
   [**\[poster\]**](https://www.cs.toronto.edu/~rupert/projects/oneshot-poster.pdf)
   [**\[arxiv\]**](https://arxiv.org/abs/2111.07138)
   [**\[code\]**](https://github.com/rhubarbwu/ENAS-Experiments)
4. **NeuralArTS: Structuring Neural Architecture Search with Type Theory**<br/>
   <u>Robert Wu</u>, [Nayan Saxena](https://uoft.me/nayan), Rohan Jain<br/>
   _AAAI 2022 (Student Abstract+Poster)_ <strong class=warning>(top 20, oral)</strong><br/>
   [**\[proceedings\]**](https://ojs.aaai.org/index.php/AAAI/article/view/21679)
   [**\[poster\]**](https://www.cs.toronto.edu/~rupert/projects/neuralarts-poster.pdf)
   [**\[arxiv\]**](https://arxiv.org/abs/2110.08710)
   [**\[code\]**](https://github.com/rhubarbwu/ENAS-Experiments)
5. **Poisoning the Search Space in Neural Architecture Search**<br/>
   <u>Robert Wu\*</u>, [Nayan Saxena](https://uoft.me/nayan)\*, Rohan Jain\*<br/>
   _ICML 2021 (AdvML Workshop)_<br/>
   [**\[openreview\]**](https://openreview.net/forum?id=fB3z4GrHCYv)
   [**\[poster\]**](https://icml.cc/media/PosterPDFs/ICML%202021/d67d8ab4f4c10bf22aa353e27879133c_Q3G0umv.png)
   [**\[arxiv\]**](https://arxiv.org/abs/2106.14406)
   [**\[code\]**](https://github.com/rhubarbwu/ENAS-Experiments)

(\* equal contribution)

---

## Other Projects

<details>
<summary>**Course Papers/Projects**</summary>

- [Custom Expressivity without the Degeneracy](./zero-pruning.pdf)
  [_(MAT1510)_](https://sites.google.com/view/mat1510)
- [Analysis of Heuristics for Neural Architecture Search](./nas-heuristics.pdf)
  [_(MAT496)_](https://artsci.calendar.utoronto.ca/course/mat496h1)
- [Q-Learning for Efficient Neural Architecture Search](./q-learning-enas.pdf)
  [_(CSC498)_](https://utm.calendar.utoronto.ca/course/csc498h5)
- [Multimodal CLIP Applications](https://github.com/rhubarbwu/Multimodal-CLIP-Applications)
  [_(CSC494)_](https://artsci.calendar.utoronto.ca/course/csc494h1)
- [Bayesian Filters State Estimation on Directed Graphs: On The Toronto Subway System](./bayesian-filters.pdf)
  [_(CSC412/2506)_](https://artsci.calendar.utoronto.ca/course/csc412h1)
- [Comparing Image Captioning Results from CNN-LSTM & Nearest-Neighbors Approaches](./image-captioning.pdf)
  [_(CSC413/2516)_](https://artsci.calendar.utoronto.ca/course/csc413h1)
- [Super-Resolution using Deep Learning](./super-resolution.pdf)
  [_(CSC420)_](https://artsci.calendar.utoronto.ca/course/csc420h1)

(\* equal contribution)

</details>

<details>
<summary>**Miscellaneous**</summary>

- Neural Collapse (Library)
  [[[github]](https://github.com/rhubarbwu/neural-collapse)]{style="float:right"}
- Conway's Game of Life in C++/CUDA
  [[[github]](https://github.com/rhubarbwu/Game-of-Life)
  [[gitlab]](https://gitlab.com/rhubarbwu/Game-of-Life)]{style="float:right"}
- [UTMIST Webpage (utmist.gitlab.io)](https://utmist.gitlab.io)
  [[[github]](https://github.com/UTMIST/utmist.gitlab.io)
  [[gitlab]](https://gitlab.com/UTMIST/utmist.gitlab.io)]{style="float:right"}
- UTMIST Runner Discord Bot
  [[[github]](https://github.com/UTMIST/MISTR)
  [[gitlab]](https://gitlab.com/UTMIST/MISTR)]{style="float:right"}

</details>
