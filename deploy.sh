#!/bin/sh

if test -f ".env"; then
    . ./.env
fi

case $1 in
"")
    rm -rf public/**.pdf
    ;;
"-f") ;;
esac

if [ -z "$1" ]; then
    echo "Error: DEPLOY_ADDRESS not provided"
    exit 1
fi
scp -r public/* $1
