#!/bin/sh

mkdir -p public

rm -rf public/**/*.pdf
case $2 in
"") ;;
"-cv")
    cp -r static/cv.pdf public/
    ;;
"-pdf")
    cd static
    pdf_files=$(find . -type f -name "*.pdf")
    echo compressing...
    for pdf_file in $pdf_files; do
        echo " " $pdf_file
        gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH -sOutputFile=../public/$pdf_file $pdf_file
    done
    cd ..
    ;;
esac

cp -r static/pt.css static/hackage.css static/$1.css static/robots.txt public/
echo generating html/css with $1 theme...
md_files=$(find . -type f -name "*.md")
for md_file in $md_files; do
    if [[ $md_file =~ "README" ]]; then
        continue
    fi
    base=$(echo $md_file | sed -e "s/^.\/content\///" -e "s/index.md$//")
    mkdir -p public/$base
    css=$(perl -e 'use File::Spec; print File::Spec->abs2rel($ARGV[0], $ARGV[1])' "public/$1.css" "public/$base")
    pandoc -s content/"$base"index.md -c $css \
        --template templates/default.html \
        >public/"$base"index.html
done
